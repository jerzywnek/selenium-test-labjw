import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.CreateAccountPage;
import pages.LoginPage;

public class Scenariusz_3_Niepoprawna_Rejestracja_Password_Format_Test extends SeleniumBaseTest {

  @DataProvider
  public Object[][] getWrongEmails() {
    return new Object[][]{
      {"test", "The Password must be at least 6 and at max 100 characters long."},
      {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
      {"test11", "Passwords must have at least one non alphanumeric character."}
    };
  }

  @Test(dataProvider = "getWrongEmails")
  public void incorrectPasswordTest(String wrongEmail, String errorMessage) {
    CreateAccountPage createAccountPage = new LoginPage(driver).goToRegister()
      .typePassword(wrongEmail)
      .typeConfirmPassword(wrongEmail)
      .typeEmail("some@test.com")
      .failureRegisterClick();
    boolean doesErrorExists = createAccountPage.registrationErrors
      .stream()
      .anyMatch(validationError -> validationError.getText().equals(errorMessage));
    Assert.assertTrue(doesErrorExists);
  }
}
