import org.testng.annotations.Test;

import pages.LoginPage;

public class Scenariusz_4_Poprawne_Logowanie_Test extends SeleniumBaseTest {

  @Test
  public void correctLoginTest() {
    new LoginPage(driver)
      .typeEmail("test@test.com")
      .typePassword("Test1!")
      .submitLogin()
      .assertWelcomeElementsIsShown();
  }
}
