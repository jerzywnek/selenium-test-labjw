import java.util.UUID;

import org.testng.annotations.Test;

import pages.LoginPage;

public class Scenariusz_1_Poprawna_Rejestracja_Test extends SeleniumBaseTest {

  @Test
  public void correctRegistrationTest() {
    String nick = UUID.randomUUID().toString().substring(0, 5);

    new LoginPage(driver)
      .goToRegister()
      .typeEmail(nick+"@test.com")
      .typePassword("Test123!")
      .typeConfirmPassword("Test123!")
      .successRegisterClick()
      .assertWelcomeElementsIsShown();
  }

}
