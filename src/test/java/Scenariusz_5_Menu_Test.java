import org.testng.annotations.Test;

import pages.LoginPage;

public class Scenariusz_5_Menu_Test extends SeleniumBaseTest {

  @Test
  public void menuProcessTest() {
    new LoginPage(driver).typeEmail("test@test.com")
      .typePassword("Test1!")
      .submitLogin()
      .goToProcesses()
      .assertProcessesUrl("http://localhost:4444/Projects")
      .assertProcessesHeader();
  }

  @Test
  public void menuCharacteristicsTest() {
    new LoginPage(driver).typeEmail("test@test.com")
      .typePassword("Test1!")
      .submitLogin()
      .goToCharacteristics()
      .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
      .assertCharacteristicsHeader();
  }

  @Test
  public void menuDashboardTest() {
    new LoginPage(driver).typeEmail("test@test.com")
      .typePassword("Test1!")
      .submitLogin()
      .goToDashboard()
      .assertDashboardUrl("http://localhost:4444/")
      .assertDemoProjectIsShown();
  }

}
