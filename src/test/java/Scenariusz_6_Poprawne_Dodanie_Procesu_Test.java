import java.util.UUID;

import org.testng.annotations.Test;

import pages.LoginPage;

public class Scenariusz_6_Poprawne_Dodanie_Procesu_Test extends SeleniumBaseTest {

  @Test
  public void addProcessTest() {
    String processName = UUID.randomUUID().toString().substring(0, 10);

    new LoginPage(driver).typeEmail("test@test.com")
      .typePassword("Test1!")
      .submitLogin()
      .goToProcesses()
      .clickAddProcess()
      .typeName(processName)
      .submitCreate()
      .assertProcess(processName, "", "");
  }

}
