import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import pages.CreateAccountPage;
import pages.LoginPage;

public class Scenariusz_2_Niepoprawna_Rejestracja_Test extends SeleniumBaseTest{

  @Test
  public void incorrectRegistrationErrorFieldTest() {
    CreateAccountPage createAccountPage = new LoginPage(driver).goToRegister()
      .typeEmail("some@test.com")
      .typePassword("Test123!")
      .typeConfirmPassword("Test123!234")
      .failureRegisterClick();
    WebElement confirmPasswordError = createAccountPage.confirmPasswordError;
    Assert.assertEquals(confirmPasswordError.getText(), "The password and confirmation password do not match.");
  }

  @Test
  public void incorrectRegistrationSummaryErrorTest() {
    CreateAccountPage createAccountPage = new LoginPage(driver).goToRegister()
      .typePassword("Test123!")
      .typeConfirmPassword("Test123!234")
      .typeEmail("some@test.com")
      .failureRegisterClick();
    boolean doesErrorExists = createAccountPage.registrationErrors
      .stream()
      .anyMatch(validationError -> validationError.getText().equals("The password and confirmation password do not match."));
    Assert.assertTrue(doesErrorExists);
  }

}
