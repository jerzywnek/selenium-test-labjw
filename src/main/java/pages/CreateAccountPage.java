package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage {

  protected WebDriver driver;

  public CreateAccountPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(id = "Email")
  private WebElement emailTxt;

  @FindBy(id = "Password")
  private WebElement passwordTxt;

  @FindBy(id = "ConfirmPassword")
  private WebElement confirmPasswordTxt;

  @FindBy(css = "button[type=submit]")
  private WebElement registerBtn;

  @FindBy(css = "#Email-error")
  public WebElement emailError;

  @FindBy(css = "#ConfirmPassword-error")
  public WebElement confirmPasswordError;

  @FindBy(css = ".validation-summary-errors>ul>li")
  public List<WebElement> registrationErrors;

  public CreateAccountPage typeEmail(String email) {
    emailTxt.clear();
    emailTxt.sendKeys(email);
    return this;
  }

  public CreateAccountPage typePassword(String password) {
    passwordTxt.clear();
    passwordTxt.sendKeys(password);
    return this;
  }

  public CreateAccountPage typeConfirmPassword(String confirmPassword) {
    confirmPasswordTxt.clear();
    confirmPasswordTxt.sendKeys(confirmPassword);
    return this;
  }

  public HomePage successRegisterClick() {
    registerBtn.click();
    return new HomePage(driver);
  }

  public CreateAccountPage failureRegisterClick() {
    registerBtn.click();
    return new CreateAccountPage(driver);
  }

}
