package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

  protected WebDriver driver;

  public LoginPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(id = "Email")
  private WebElement emailTxt;

  @FindBy(id = "Password")
  private WebElement passwordTxt;

  @FindBy(css = "button[type=submit]")
  private WebElement loginBtn;

  @FindBy(css = "#Email-error")
  public WebElement emailError;

  @FindBy(css = ".validation-summary-errors>ul>li")
  public List<WebElement> loginErrors;

  @FindBy(linkText = "Register as a new user?")
  private WebElement goToRegistrationLink;

  public LoginPage typeEmail(String email) {
    emailTxt.clear();
    emailTxt.sendKeys(email);
    return this;
  }

  public LoginPage typePassword(String password) {
    passwordTxt.clear();
    passwordTxt.sendKeys(password);
    return this;
  }

  public HomePage submitLogin() {
    loginBtn.click();
    return new HomePage(driver);
  }

  public CreateAccountPage goToRegister() {
    goToRegistrationLink.click();
    return new CreateAccountPage(driver);
  }

}
