package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardPage extends HomePage {

  public DashboardPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = ".x_title>h2")
  private WebElement dashboardElm;

  public DashboardPage assertDashboardUrl(String pageUrl) {
    Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
    return this;
  }

  public DashboardPage assertDemoProjectIsShown(){
    Assert.assertTrue(dashboardElm.isDisplayed(), "DEMO PROJECT element is not shown.");
    Assert.assertTrue(dashboardElm.getText().contains("DEMO PROJECT"));
    return this;
  }
}
