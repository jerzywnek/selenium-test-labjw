package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

  private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

  public ProcessesPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = ".page-title h3")
  private WebElement pageHeader;

  @FindBy(css = "a[href$=Create]")
  private WebElement addNewProcessBtn;

  @FindBy(id = "Name")
  private WebElement nameField;

  @FindBy(css = "input[type=submit]")
  private WebElement createBtn;

  public ProcessesPage clickAddProcess() {
    addNewProcessBtn.click();
    return new ProcessesPage(driver);
  }

  public ProcessesPage typeName(String name) {
    nameField.clear();
    nameField.sendKeys(name);
    return this;
  }

  public ProcessesPage submitCreate() {
    createBtn.click();
    return new ProcessesPage(driver);
  }

  public ProcessesPage assertProcessesHeader() {
    Assert.assertEquals(pageHeader.getText(), "Processes");
    return this;
  }

  public ProcessesPage assertProcessesUrl(String pageUrl) {
    Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
    return this;
  }

  public ProcessesPage assertProcess(String expName, String expDescription, String expNotes) {
    String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);

    WebElement processRow = driver.findElement(By.xpath(processXpath));

    String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
    String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

    Assert.assertEquals(actDescription, expDescription);
    Assert.assertEquals(actNotes, expNotes);

    return this;
  }

}
