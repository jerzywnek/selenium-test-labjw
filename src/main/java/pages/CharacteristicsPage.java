package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

  public CharacteristicsPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = ".page-title h3")
  private WebElement pageHeader;

  public CharacteristicsPage assertCharacteristicsHeader() {
    Assert.assertEquals(pageHeader.getText(), "Characteristics");
    return this;
  }

  public CharacteristicsPage assertCharacteristicsUrl(String pageUrl) {
    Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
    return this;
  }
}
